/*
Copyright 2021 The SHUMIN Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package goios

import "fmt"

/*
	切片测试
*/
type Docker struct {
	Name  string
	Image string
}

func NewDocker(name, image string) *Docker {
	return &Docker{
		Name:  name,
		Image: image,
	}
}

func (s *Docker) Get() *HistoryData {
	m := make([]string, 0)
	m = append(m, s.Name)
	m = append(m, s.Image)
	return &HistoryData{messages: m}
}

//  HistoryData 解决过度方案
type HistoryData struct {
	messages []string
}

func (d *HistoryData) NumMessages() int {
	return len(d.messages)
}
func (d *HistoryData) MessageAt(i int) string {
	return d.messages[i]
}

// 不支持
func (s *Docker) SetMap(mp map[string]string) {
	fmt.Println("SetMap :", mp)
}

// 不支持
func (s *Docker) SetArray(sr []string) {
	fmt.Println("Set Array: ", sr)
}

// Map 测试 不支持
// skipped method Docker.GetMap with unsupported parameter or return types
func (s *Docker) GetMap() (map[string]string, error) {
	panic("implement me")
}

// 切片测试 不支持
// skipped method Docker.GetMap with unsupported parameter or return types
func (s *Docker) GetArrays() ([]int, error) {
	return []int{1, 2, 3}, nil
}
