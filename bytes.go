/*
Copyright 2021 The SHUMIN Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package goios

import "errors"

/*
	bytes 切片测试
*/
func GetBytes() []byte {
	return []byte("i am is bytes")
}

/*
	错误测试
*/

func GetError() (string, error) {
	return "", errors.New("i am golang error")
}
