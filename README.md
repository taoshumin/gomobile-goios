## Usage

**简介； 主要是测试gomobile打包成xcframewrok支持的数据类型。经过测试发现slice,map,interface等类型不能作为传入参数和传出参数。**


[https://github.com/golang/go/wiki/Mobile](https://github.com/golang/go/wiki/Mobile)

```
$ cd goios 
$ go mod init 
$ go install golang.org/x/mobile/cmd/gomobile@latest
$ gomobile init
$ go get -d golang.org/x/mobile/example/bind/...
$ sudo gomobile bind -target=ios  x6t.io/goios
$ sudo gomobile bind -target macos x6t.io/goios
```

## 打包流程

[参考: https://gitlab.com/taoshumin/holy](https://gitlab.com/taoshumin/holy)
